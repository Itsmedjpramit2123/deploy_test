export default ({ app }, inject) => {
    const getMessage = () => {
        return process.env.MESSAGE   
    };  
    inject("getMessage", getMessage);
  }